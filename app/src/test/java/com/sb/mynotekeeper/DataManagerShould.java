package com.sb.mynotekeeper;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataManagerShould {

    private CourseInfo _course;
    private String _noteTitle;
    private String _noteText;
    private static DataManager _sDm;

    @BeforeClass
    public static void classSetUp(){
        _sDm = DataManager.getInstance();
    }

    @Before
    public void setUp(){
        _sDm.getNotes().clear();
        _sDm.initializeExampleNotes();

        _course = _sDm.getCourse("android_async");
        _noteTitle = "Test note title";
        _noteText = "This is the body text of my note";
    }

    @Test
    public void createNewNote() {

        int noteIndex = _sDm.createNewNote();
        NoteInfo newNote = _sDm.getNotes().get(noteIndex);
        newNote.setCourse(_course);
        newNote.setTitle(_noteTitle);
        newNote.setText(_noteText);

        NoteInfo compareNote = _sDm.getNotes().get(noteIndex);


        assertEquals(_course, compareNote.getCourse());
        assertEquals(_noteTitle, compareNote.getTitle());
        assertEquals(_noteText, compareNote.getText());
    }

    @Test
    public void findSimilarNotes(){

        String noteText1 = _noteText;
        String noteText2 = "This is the body text of my second note";

        int noteIndex1 = _sDm.createNewNote();
        NoteInfo newNote1 = _sDm.getNotes().get(noteIndex1);
        newNote1.setCourse(_course);
        newNote1.setTitle(_noteTitle);
        newNote1.setText(noteText1);

        int noteIndex2 = _sDm.createNewNote();
        NoteInfo newNote2 = _sDm.getNotes().get(noteIndex2);
        newNote2.setCourse(_course);
        newNote2.setTitle(_noteTitle);
        newNote2.setText(noteText2);

        int foundIndex1 = _sDm.findNote(newNote1);
        assertEquals(noteIndex1, foundIndex1);


        int foundIndex2 = _sDm.findNote(newNote2);
        assertEquals(noteIndex2, foundIndex2);

    }

    @Test
    public void createNewNoteOneStepCreation(){
        int noteIndex = _sDm.createNewNote(_course, _noteText, _noteTitle);
        NoteInfo compareNote = _sDm.getNotes().get(noteIndex);

        assertEquals(_course, compareNote.getCourse());
        assertEquals(_noteText, compareNote.getText());
        assertEquals(_noteTitle, compareNote.getTitle());
    }
}