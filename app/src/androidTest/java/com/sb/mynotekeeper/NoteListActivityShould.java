package com.sb.mynotekeeper;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.*;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.ViewAssertion.*;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class NoteListActivityShould {

    private static DataManager _dm;

    @BeforeClass
    public static void classSetUp(){
        _dm = DataManager.getInstance();
    }
    @Rule
    public ActivityTestRule<NoteListActivity> noteListActivityRule = new ActivityTestRule<>(NoteListActivity.class);

    @Test
    public void createNewNote(){
        final CourseInfo course = _dm.getCourse("java_lang");
        final String noteTitle = "Test note title";
        final String noteText = "This is the body of our test note";

        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.spinner_courses)).perform(click());

        onData(allOf(instanceOf(CourseInfo.class), equalTo(course))).perform(click());
        onView(withId(R.id.spinner_courses)).check(
                matches(
                        withSpinnerText(
                                containsString(course.getTitle())
                        )
                )
        );

        onView(withId(R.id.text_note_title)).perform(typeText(noteTitle)).check(matches(withText(containsString(noteTitle))));
        onView(withId(R.id.text_note_text)).perform(typeText(noteText),
                closeSoftKeyboard());
        onView(withId(R.id.text_note_text)).check(matches(withText(containsString(noteText))));

        pressBack();

        int createdNoteIndex = _dm.getNotes().size() - 1;
        NoteInfo createdNote = _dm.getNotes().get(createdNoteIndex);

        assertEquals(course, createdNote.getCourse());
        assertEquals(noteText, createdNote.getText());
        assertEquals(noteTitle, createdNote.getTitle());

    }
}