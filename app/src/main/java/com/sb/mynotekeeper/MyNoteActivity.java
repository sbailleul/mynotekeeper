package com.sb.mynotekeeper;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

public class MyNoteActivity extends AppCompatActivity {

    public static final String NOTE_POSITION = "com.sb.mynotekeeper.NOTE_POSITION";
    public static final int NOT_SET_POSITION = -1;
    private NoteInfo _note;
    private boolean _isNewNote;
    private Spinner _spinnerCourses;
    private EditText _textNoteTitle;
    private EditText _textNoteText;
    private int _notePosition;
    private boolean _isCancelling;
    private MyNoteActivityViewModel _viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mynote);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewModelProvider viewModelProvider = new ViewModelProvider(getViewModelStore(), ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()));
        _viewModel = viewModelProvider.get(MyNoteActivityViewModel.class);

        if(savedInstanceState != null && _viewModel.isNewlyCreated){
            _viewModel.restoreState(savedInstanceState);
        }
        _viewModel.isNewlyCreated = false;

        _spinnerCourses = findViewById(R.id.spinner_courses);

        List<CourseInfo> courses = DataManager.getInstance().getCourses();
        ArrayAdapter<CourseInfo> adapterCourses =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, courses);
        adapterCourses.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _spinnerCourses.setAdapter(adapterCourses);

        readDisplayStateValues();
        saveOriginalNoteValues();

        _textNoteTitle = findViewById(R.id.text_note_title);
        _textNoteText = findViewById(R.id.text_note_text);

        if (!_isNewNote)
            displayNote(_spinnerCourses, _textNoteTitle, _textNoteText);
    }

    private void saveOriginalNoteValues() {
        if (_isNewNote) return;
        _viewModel.originalNoteCourseId = _note.getCourse().getCourseId();
        _viewModel.originalNoteTitle = _note.getTitle();
        _viewModel.originalNoteText = _note.getText();
    }

    private void displayNote(Spinner spinnerCourses, EditText textNoteTitle, EditText textNoteText) {
        List<CourseInfo> courses = DataManager.getInstance().getCourses();
        int courseIndex = courses.indexOf(_note.getCourse());
        spinnerCourses.setSelection(courseIndex);
        textNoteText.setText(_note.getText());
        textNoteTitle.setText(_note.getTitle());
    }

    private void readDisplayStateValues() {
        Intent intent = getIntent();
        final int notePosition = intent.getIntExtra(NOTE_POSITION, NOT_SET_POSITION);
        _isNewNote = notePosition == NOT_SET_POSITION;

        if(_isNewNote){
            createNewNote();
        } else {
            _note = DataManager.getInstance().getNotes().get(notePosition);
        }
    }

    private void createNewNote() {
        DataManager dm = DataManager.getInstance();
        _notePosition = dm.createNewNote();
        _note = dm.getNotes().get(_notePosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mynote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_send_mail) {
            sendEmail();
            return true;
        } else if (id == R.id.action_cancel) {
            _isCancelling = true;
            finish();
        } else if (id == R.id.action_next){
            moveNext();
        }


        return super.onOptionsItemSelected(item);
    }

    private void moveNext() {
        ++_notePosition;
        _note = DataManager.getInstance().getNotes().get(_notePosition);

        displayNote(_spinnerCourses, _textNoteTitle, _textNoteText);
    }

    private void sendEmail() {
        CourseInfo course = (CourseInfo) _spinnerCourses.getSelectedItem();
        String subject = _textNoteTitle.getText().toString();
        String text = "Checkout what I learned in the Pluralsight course \"" + course.getTitle() + "\"\n" + _textNoteText.getText();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc2822");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_isCancelling) {
            if (_isNewNote) {
                DataManager.getInstance().removeNote(_notePosition);
            } else {
                storePreviousNoteValues();
            }
        } else {
            saveNote();
        }
    }

    private void storePreviousNoteValues() {
        CourseInfo originalNoteCourse = DataManager.getInstance().getCourse(_viewModel.originalNoteCourseId);
        _note.setCourse(originalNoteCourse);
        _note.setText(_viewModel.originalNoteText);
        _note.setTitle(_viewModel.originalNoteTitle);
    }

    private void saveNote() {
        _note.setCourse((CourseInfo) _spinnerCourses.getSelectedItem());
        _note.setTitle(_textNoteTitle.getText().toString());
        _note.setText(_textNoteText.getText().toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(outState != null) _viewModel.saveState(outState);
    }
}
