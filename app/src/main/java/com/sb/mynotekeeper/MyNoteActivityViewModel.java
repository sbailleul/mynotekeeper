package com.sb.mynotekeeper;

import android.os.Bundle;

import androidx.lifecycle.ViewModel;

public class MyNoteActivityViewModel extends ViewModel {

    public static final String ORIGINAL_NOTE_COURSE_ID = "com.sb.mynotekeeper.ORIGINAL_NOTE_COURSE_ID";
    public static final String ORIGINAL_NOTE_TITLE = "com.sb.mynotekeeper.ORIGINAL_NOTE_TITLE";
    public static final String ORIGINAL_NOTE_TEXT = "com.sb.mynotekeeper.ORIGINAL_NOTE_TEXT";

    public String originalNoteCourseId;
    public String originalNoteTitle;
    public String originalNoteText;
    public boolean isNewlyCreated = true;


    public void saveState(Bundle outState) {
        outState.putString(ORIGINAL_NOTE_COURSE_ID, originalNoteCourseId);
        outState.putString(ORIGINAL_NOTE_TEXT, originalNoteText);
        outState.putString(ORIGINAL_NOTE_TITLE, originalNoteTitle);
    }

    public void restoreState(Bundle inState){
        originalNoteCourseId = inState.getString(ORIGINAL_NOTE_COURSE_ID);
        originalNoteText = inState.getString(ORIGINAL_NOTE_TITLE);
        originalNoteTitle= inState.getString(ORIGINAL_NOTE_TEXT);
    }
}
