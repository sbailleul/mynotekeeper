package com.sb.mynotekeeper;

import android.os.Parcel;
import android.os.Parcelable;

public final class NoteInfo implements Parcelable {
    private CourseInfo _course;
    private String _title;
    private String _text;

    public NoteInfo(CourseInfo course, String title, String text) {
        _course = course;
        _title = title;
        _text = text;
    }

    private NoteInfo(Parcel source) {
        _course = source.readParcelable(CourseInfo.class.getClassLoader());
        _title = source.readString();
        _text = source.readString();
    }

    public CourseInfo getCourse() {
        return _course;
    }

    public void setCourse(CourseInfo course) {
        _course = course;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        _text = text;
    }

    private String getCompareKey() {
        return _course.getCourseId() + "|" + _title + "|" + _text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NoteInfo that = (NoteInfo) o;

        return getCompareKey().equals(that.getCompareKey());
    }

    @Override
    public int hashCode() {
        return getCompareKey().hashCode();
    }

    @Override
    public String toString() {
        return getCompareKey();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeParcelable(_course, 0);
        dest.writeString(_title);
        dest.writeString(_text);
    }

    public static final Creator<NoteInfo> CREATOR = new Creator<NoteInfo>() {

        @Override
        public NoteInfo createFromParcel(Parcel source) {
            return new NoteInfo(source);
        }

        @Override
        public NoteInfo[] newArray(int size) {
            return new NoteInfo[size];
        }
    };
}
